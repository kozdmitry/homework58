import React from "react";
import "./Alert.css";
const Alert = (props) => {
    if (props.dismiss === undefined) {
        return (
            <>
                <div
                    className={["Alert", props.type].join(" ")}
                    style={{
                        display: props.show ? 'block' : 'none'
                    }}
                >
                    {props.children}
                </div>
            </>
        );
    }

    return (
        <>
            <div
                className={["Alert", props.type].join(" ")}
                style={{
                    display: props.show ? 'block' : 'none'
                }}
            >
                {props.children}
                <button onClick={props.dismiss}>Close</button>
            </div>
        </>
    );
};

export default Alert;
