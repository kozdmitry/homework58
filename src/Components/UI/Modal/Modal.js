import React from "react";
import Backdrop from "../Backdrop/Backdrop";
import "./Modal.css";

const Modal = (props) => {
    return (
        <>
            <Backdrop show={props.show} onClick={props.closed} />
            <div
                className="Modal"
                style={{
                    display: props.show ? 'block' : 'none'
                }}
            >
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">{props.title}</h5>
                    </div>
                    <div className="modal-body">{props.children}</div>
                    <div className="modal-footer">
                        <button className="btn btn-outline-success" onClick={props.continued}>
                            Сontinue
                        </button>
                        <button className="btn btn-outline-primary" onClick={props.closed}>
                            Close
                        </button>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Modal;
