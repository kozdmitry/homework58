import React, { useState } from "react";
import "./App.css";
import Modal from "./Components/UI/Modal/Modal";
import Alert from "./Components/UI/Alert/Alert";


const App = () => {
    const [showModal, setShowModal] = useState(false);
    const [showAlert, setShowAlert] = useState(false);

    const showNewModal = () => {
        setShowModal(true);
    };
    const closedNewModal = () => {
        setShowModal(false);
    };
    const continuedModal = () => {
        alert("Your continued");
        setShowModal(false);
    };
    const showNewAlert = () => {
        setShowAlert(true);
    };
    const closedNewAlert = () => {
        setShowAlert(false);
    };

    return (
        <div className="App">
            <Modal
                show={showModal}
                title="Some kinda modal title"
                closed={closedNewModal}
                continued={continuedModal}
            >
                <p>This is modal content</p>
            </Modal>

            <Alert show={showAlert} type="warning">
                This is a warning type alert
            </Alert>
            <Alert show={showAlert} type="danger" dismiss={closedNewAlert}>
                This is a danger type alert
            </Alert>
            <button className="btn btn-outline-primary" onClick={showNewModal}>
                Show Modal
            </button>
            <button className="btn btn-outline-danger" onClick={showNewAlert}>
                Show Alert
            </button>
        </div>
    );
};

export default App;
